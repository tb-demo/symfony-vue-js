# Project Setup 
### Clone Repository.

### Change Directory
- `cd symfony_with_vuejs_demo`

### Install composer package
- `Composer install`

### Copy .env file
- `cp .env.example .env`

### Update the database connection in .env file
- `Ex: DATABASE_URL="mysql://root:1234@127.0.0.1:3306/sfv_demo?serverVersion=5.7&charset=utf8mb4"`

### To create dummy user
- `php bin/console doctrine:fixtures:load`

### Run the database migration
- `php bin/console doctrine:schema:update --force`

### Start symfony server
- `symfony server:start -d` | `php bin/console server:run`

### Install node packages
- `sudo yarn install`

### Build the frontend
- `sudo yarn watch`


### Credentials
- `Username: demo`
- `Password: demo`